# OAuth2Proxy for Home Assistant

This is a small, java-based proxy that allows you to protect your Home Assistant installation with OAuth2.
Software is pre-configured to be used with Google OAuth, although it should work just fine with for example Facebook,
provided appropriate parameters are set (see `src/main/resources/application.yml`).

This service is Spring Cloud + Spring Boot based. You can use properties provided by spring to customise port, logging
and additional options.

## Required properties

```properties
CLIENT_ID=<Client Id from Google Cloud>
CLIENT_SECRET=<Client Secret>
USERS='foobar@gmail.com', 'john.mccain@gmail.com' # Users that should have access to HA
KEY=000-123123-0000-343434-12312 # Secret Key to be used for API access in automations
HOSTNAME=https://home.example.com # Your home hostname here
```

## Supported auth schemas

This proxy supports a number of auth schemas, allowing you to both access the UI using OAuth and write a number of automations
using third party software that do not supports OAuth.

1. OAuth2 - primary auth schema
2. Basic Auth - login: homeassistant; password is same as set in `$KEY` property.
3. Header Auth - Header with name `key` and value same as set in `$KEY` property.
4. Request parameter Auth - name of parameter is `key` and value same as set in `$KEY` property.

## Installation

This description will walk you through configuration, installation of the proxy as a service and configuration of logging.
Read more about the configuration in [blog.luciow.pl](https://blog.luciow.pl/automation/2018/02/10/dont-reinvent-the-wheel/)

```bash
mkdir -p /opt/oauth2proxy/config
## Replace 0.1.0 with chosen version
wget -O /tmp/archive.jar https://gitlab.com/api/v4/projects/gyatso-home-automation%2Foauth2proxy/jobs/artifacts/0.1.0/download?job=release
unzip /tmp/archive.jar -d /opt/oauth2proxy/
cat <<EOF > /opt/oauth2proxy/config/application.properties
CLIENT_ID=<Client Id from Google Cloud>
CLIENT_SECRET=<Client Secret>
USERS='foobar@gmail.com', 'john.mccain@gmail.com' # Users that should have access to HA
KEY=000-123123-0000-343434-12312 # Secret Key to be used for API access in automations
HOSTNAME=https://home.example.com # Your home hostname here
EOF

cat <<EOF > /etc/systemd/system/oauth2proxy.service
 
[Unit]
Description=oauth2proxy service
After=syslog.target

[Service]
Type=simple
ExecStart=/opt/oauth2proxy/app.jar
Restart=always
RestartSec=30
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=oauth2proxy  
 
[Install]
WantedBy=multi-user.target
 
EOF
 
systemctl daemon-reload
systemctl enable oauth2proxy.service
systemctl start oauth2proxy.service

cat <<EOF > /etc/rsyslog.d/25-oauth2proxy.conf
if $programname == 'oauth2proxy' then /var/log/oauth2proxy
if $programname == 'oauth2proxy' then stop
EOF
sudo systemctl restart rsyslog
```

## Using behind https proxy

You can choose to use this proxy behind https proxy, for instance NGINX. In this case, remeber to set websocket proxing as well.

```
upstream websocket {
    server 127.0.0.1:8080;
}

# inside your server
    location /api/websocket {
        proxy_pass http://websocket;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";

          proxy_set_header        Host $host;
          proxy_set_header        X-Real-IP $remote_addr;
          proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
          proxy_set_header        X-Forwarded-Proto $scheme;
    }
```

## Actuator endpoints 

By default this application exposes two actuator endpoints, the [/actuator/info](https://docs.spring.io/spring-boot/docs/current/actuator-api/html/#info) 
and [/actuator/auditevents](https://docs.spring.io/spring-boot/docs/current/actuator-api/html/#audit-events).

You can consume these endpoints in Home Assistant using [Rest Sensor](https://www.home-assistant.io/components/sensor.rest/) 
to browse through audit events and show version of the proxy. 

To show current, installed version of proxy and the latest available add these two sensors:

```yaml
sensor:
  - platform: rest
    resource: http://127.0.0.1:8080/actuator/info
    name: OAuth2 Proxy Version
    value_template: '{{ value_json.build.version }}'
    headers:
      key: !secret proxy_key
  - platform: rest
    resource: https://gitlab.com/api/v4/projects/5431713/repository/tags
    name: OAuth2 Latest Proxy Version
    value_template: '{{value_json[0].name}}'
```

You can also send a notification whenever the new version is published

```yaml
- alias: 'OAuth2Proxy Update Available Notifications'
  initial_state: 'on'
  trigger:
    platform: time
    hours: 12
    minutes: 00
    seconds: 00
  condition:
    condition: template
    value_template: "{{states.sensor.oauth2_latest_proxy_version.state != states.sensor.oauth2_proxy_version.state}}"
  action:
    service: notify.email_alert
    data:
      title: 'An update is avialable'
      message: >
        Update for OAuth2Proxy ({{states.sensor.oauth2_latest_proxy_version.state}}) is available.
```

## License

This software is distributed under `Apache License, Version 2.0`

See the license [here](https://opensource.org/licenses/Apache-2.0).