package pl.luciow.home.oauth2proxy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import pl.luciow.home.oauth2proxy.config.ControllerConfigurationProperties;

@EnableWebSocket
@EnableZuulProxy
@SpringBootApplication
@EnableConfigurationProperties(ControllerConfigurationProperties.class)
public class ControllerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ControllerApplication.class, args);
    }
}
