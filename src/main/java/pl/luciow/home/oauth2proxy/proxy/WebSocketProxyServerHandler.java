package pl.luciow.home.oauth2proxy.proxy;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;
import org.springframework.web.util.UriComponentsBuilder;
import pl.luciow.home.oauth2proxy.config.ControllerConfigurationProperties;

import javax.websocket.ContainerProvider;
import javax.websocket.WebSocketContainer;
import java.util.concurrent.TimeUnit;

@Component
@RequiredArgsConstructor
public class WebSocketProxyServerHandler extends AbstractWebSocketHandler {

    private final ControllerConfigurationProperties properties;
    private WebSocketSession webSocketClientSession;

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        this.webSocketClientSession = createWebSocketClientSession(session, properties.getHomeAssistantAddress());
    }

    @Override
    public void handleMessage(WebSocketSession webSocketSession, WebSocketMessage<?> webSocketMessage) throws Exception {
        webSocketClientSession.sendMessage(webSocketMessage);
    }

    private WebSocketSession createWebSocketClientSession(WebSocketSession webSocketServerSession, String homeAssistantAddress) {
        WebSocketHttpHeaders webSocketHttpHeaders = new WebSocketHttpHeaders(webSocketServerSession.getHandshakeHeaders());
        try {

            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            container.setDefaultMaxTextMessageBufferSize(1024 * 1024);
            container.setDefaultMaxBinaryMessageBufferSize(1024 * 1024);

            return new StandardWebSocketClient(container)
                    .doHandshake(new WebSocketProxyClientHandler(webSocketServerSession),
                            webSocketHttpHeaders,
                            UriComponentsBuilder.fromUriString("ws://" + homeAssistantAddress + "/api/websocket?latest").build().toUri()
                    )
                    .get(1000, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}