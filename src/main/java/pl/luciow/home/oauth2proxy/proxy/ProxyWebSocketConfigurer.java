package pl.luciow.home.oauth2proxy.proxy;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import pl.luciow.home.oauth2proxy.config.ControllerConfigurationProperties;

@Component
@RequiredArgsConstructor
public class ProxyWebSocketConfigurer implements WebSocketConfigurer {

    private final ControllerConfigurationProperties properties;

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(new WebSocketProxyServerHandler(properties), "/api/websocket");
    }

}
