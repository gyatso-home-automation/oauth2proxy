package pl.luciow.home.oauth2proxy.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("custom")
public class ControllerConfigurationProperties {

    private String allowedUsers;
    private String homeAssistantAddress;
    private String key;
    private String username;
}
