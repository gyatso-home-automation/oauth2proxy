package pl.luciow.home.oauth2proxy.config;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import static com.netflix.zuul.context.RequestContext.getCurrentContext;

@Service
public class AccessTokenZuulFilter extends ZuulFilter {

    @Value("${accesstoken:none}")
    private String accessToken;

    public String filterType() {
        return "pre";
    }

    public int filterOrder() {
        return 6;
    }

    public boolean shouldFilter() {
        return !"none".equals(accessToken);
    }

    public Object run() {
        RequestContext context = getCurrentContext();
        context.addZuulRequestHeader("Authorization", "Bearer " + accessToken);

        return null;
    }


}
