package pl.luciow.home.oauth2proxy.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter;

@Slf4j
@Configuration
public class AuthConfigurer extends WebSecurityConfigurerAdapter {

    @Autowired
    private ControllerConfigurationProperties properties;

    @Value("${success:/states}")
    private String defaultSuccessUrl;

    @Value("${always-redirect:true}")
    private boolean alwaysRedirect;

    @Override
    public void configure(HttpSecurity http) throws Exception {

        http.addFilterBefore(new KeyFilter(properties), AnonymousAuthenticationFilter.class);

        http.authorizeRequests()
                .antMatchers("/img/**/*", "/api/tts_proxy/*", "/login.html", "/failed.html", "/service_worker.js", "/frontend_latest/**/*", "/static/**").permitAll()
                .antMatchers("/**").authenticated();

        http.csrf().disable();

        http.authorizeRequests().antMatchers("/**")
                .access("(authentication instanceof T(org.springframework.security.authentication.UsernamePasswordAuthenticationToken)) || " +
                        "authentication instanceof T(pl.luciow.home.oauth2proxy.config.KeyAuthentication) || " +
                        "(authentication instanceof T(org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken) && {"
                        + properties.getAllowedUsers() + "}.contains(authentication.getPrincipal().getClaims().get('email')))");

        http.authorizeRequests()
                .anyRequest().authenticated()
                .and()
                .authenticationProvider(new UsernamePasswordAuthProvider(properties))
                .logout().logoutSuccessUrl("/login.html")
                .and()
                .oauth2Login()
                .loginPage("/login.html")
                .failureUrl("/failed.html")
                .defaultSuccessUrl(defaultSuccessUrl, alwaysRedirect);

        super.configure(http);
    }
}
