package pl.luciow.home.oauth2proxy.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.Principal;

@Slf4j
@RequiredArgsConstructor
public class KeyFilter extends OncePerRequestFilter {

    private static final String KEY_HEADER_NAME = "key";
    private static final String AUTHORIZATION_HEADER_NAME = "authorization";
    private static final String PARAMETER_NAME = "key";

    private final ControllerConfigurationProperties properties;
    private final AuthenticationEntryPoint authenticationEntryPoint = new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED);

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        if (SecurityContextHolder.getContext().getAuthentication() == null) {
            String key = request.getHeader(KEY_HEADER_NAME);
            if (!StringUtils.hasText(key)) {
                log.debug("Fallback to second header");
                key = request.getHeader(AUTHORIZATION_HEADER_NAME);
                if (StringUtils.hasText(key)) {
                    key = key.replaceAll("Bearer ", "");
                    key = key.replaceAll("bearer ", "");
                }
            }
            if (!StringUtils.hasText(key)) {
                log.debug("Fallback to parameters");
                key = request.getParameter(PARAMETER_NAME);
            }
            if (StringUtils.hasText(key)) {
                if (!key.equals(properties.getKey())) {
                    authenticationEntryPoint.commence(request, response,
                            new InsufficientAuthenticationException("Invalid key"));
                } else {
                    Authentication authResult = new KeyAuthentication();
                    SecurityContextHolder.getContext().setAuthentication(authResult);
                    try {
                        wrapRequest(filterChain, request, response, SecurityContextHolder.getContext().getAuthentication());
                    } finally {
                        HttpSession session = request.getSession(false);
                        if (session != null) {
                            session.invalidate();
                        }
                        SecurityContextHolder.clearContext();
                    }
                }
            } else {
                filterChain.doFilter(request, response);
            }
        } else {
            filterChain.doFilter(request, response);
        }
    }

    private void wrapRequest(FilterChain next, HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException, IOException {
        HttpServletRequest requestWrapper = new HttpServletRequestWrapper(request) {

            @Override
            public Principal getUserPrincipal() {
                return authentication;
            }

            @Override
            public String getRemoteUser() {
                return getUserPrincipal().getName();
            }
        };
        next.doFilter(requestWrapper, response);
    }
}
