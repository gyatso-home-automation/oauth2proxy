package pl.luciow.home.oauth2proxy.config;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/auth/googleassistant")
@RequiredArgsConstructor
public class GoogleAssistantController {

    private final ControllerConfigurationProperties properties;

    @GetMapping
    public void getAccessToken(@RequestParam("redirect_uri") String redirectUri, @RequestParam("state") String state,
                               HttpServletResponse httpServletResponse) {
        httpServletResponse.setHeader("Location",
                redirectUri +
                        "#access_token=" + properties.getKey() +
                        "&token_type=Bearer" +
                        "&state=" + state
        );
        httpServletResponse.setStatus(302);

    }

}
